# Éducajou

**[Éducajou](https://educajou.forge.apps.education.fr/) est une collection d'applications d'intérêt éducatif, libres et gratuites.**
Réalisées sur et pour le terrain, elles s'adressent avant tout à l'école primaire, bien que certaines peuvent être utiles dans d'autres contextes. Pour la plupart il s'agit d'applications en ligne sans installation ni inscription, qui fonctionnent sur ordinateur, tablette voire écran numérique interactif.
Il s'agit aussi d'un projet multimodal. Certaines de ces applications sont déployées comme [applications Openboard](https://openboard.forge.apps.education.fr/applications/) ainsi que dans [Primtux](https://primtux.fr/), voire adaptées sur [La Digitale](https://ladigitale.dev/). Le choix de la licence libre facilite ces réutilisations.
Éducajou est un projet développé par Arnaud Champollion, conseiller pédagogique numérique. Il est partagé sur la [Forge des Communs Numériques Éducatifs](https://forge.apps.education.fr/), de Apps Éducation.

## Mathématiques

### Tuxblocs
Tuxblocs est un logiciel de médiation qui permet à l'enseignant de travailler avec ses élèves la numération positionnelle, au niveau représenté. C'est l'étape intermédiaire entre la manipulation et l'écriture chiffrée.
Les unités, dizaines, centaines et milliers figurent sous forme de blocs que l'on peut regrouper et casser. Possibilité de jouer avec les variables didactiques (masquer les détails des blocs, les représentations chiffrées ou les blocs eux-mêmes, mettre en évidence les colonnes supérieures à 9...) et d'utiliser des fonctions de tirages aléatoires.
*Ce logiciel est à télécharger et à installer (Linux & Windows).
Des fiches-exemples de séance accompagnent Tuxblocs.*

### Fracatux
Fracatux est l'équivalent de Tuxblocs pour la représentation des fractions sous forme de barres.
L'accent est mis sur les fractions unitaires (celles dont le numérateur est un) comme unités de base des autres fractions, par itération. Ainsi 3/5 c'est 1/5 + 1/5 + 1/5 + 1/5 +1/5. Voir "Putting fractions together" par Davis Braithwaite & Robert Siegler.
On peut diviser une fraction-barre en plusieurs parties, les regrouper, afficher différentes écritures chiffrées (fractionnaire, décimale ...) et les positionner sur une droite numérique.
*Ce logiciel est à télécharger et à installer (Linux & Windows).
Des fiches-exemples de séance accompagnent Fracatux.*

### Compteur
Compteur permet d'afficher t nombre et d'en faire défiler les chiffres, façon "compteur kilométrique".
En calcul réfléchi, il permet de mettre en évidence, par exemple, qu'effectuer l'opération "+30" c'est ajouter 3 dizaines, et donc décaler de 3 chiffres le compteur des dizaines.
Possibilité de personnaliser le nombre de cases affichées.
*Cette application fonctionne en ligne sans installation (tous appareils).
On peut d'ailleurs commander un nombre directement en personnalisant le lien (il suffit d'ajouter ?nombre= suivi du nombre).
Exemple : https://educajou.forge.apps.education.fr/compteur?nombre=10,25*
*Elle existe en version intégrable à Openboard.*

### Droite interactive
Droite interactive permet simplement d'afficher une droite graduée personnalisable, et d'y positionner des blocs de type "cuisineaire" ou ponts.
On peut ainsi travailler sur la relation entre ordinal et cardinal.
*Cette application fonctionne en ligne sans installation (tous appareils).
On peut d'ailleurs régler les valeurs directement en personnalisant le lien, sur le modèle ci-dessous :
https://educajou.forge.apps.education.fr/droite-interactive/?min=0&max=25&allera=14
Ce lien affichera directement une droite allant de 0 à 25, centrée sur 14.*

### Automultiples
Automultiples est une application minimaliste, il s'agit d'une calculatrice à multiples.
Elle affiche simplement les multiples d'un nombre. À utiliser notamment pour la technique opératoire de la division, afin d'avoir rapidement sous la main les multiples nécessaires.
*Cette application fonctionne en ligne sans installation (tous appareils).
On peut d'ailleurs commander un nombre directement en personnalisant le lien (il suffit d'ajouter # suivi du nombre).
Exemple : https://educajou.forge.apps.education.fr/automultiples/#42 pour afficher les multiples de 42.*
*Elle existe en version intégrable à Openboard.*

### Tétrimath
Tétrimath est une application ludique sur le thème des décompositions additives de 10,20 ou 100, avec un gameplay de type Tétris (blocs qui tombent).
L'élève est invité à faire se toucher des blocs, par exemple pour totaliser 10 (par exemple 3+7). Un bonus est attribué en cas de réalisation de plusieurs dizaines simultanées (donc de multiples de 10, par exemple 3+8+7+2).
*Cette application fonctionne en ligne sans installation (tous appareils).*

### Estimation
Dans Estimation, une droite non graduée de 0 à 10 est affichée, il faut indiquer le nombre d'une position donnée.
Plusieurs étayages sont à la disposition de l'élève. Possibilité de personnaliser les limites de la droite.
*Cette application fonctionne en ligne sans installation (tous appareils).*

### Multiples
Multiples permet de représenter les multiplications sous forme de quadrillage.
On peut masquer un facteur, les deux ou le résultat, afin d'interroger les élèves dans les deux sens sur les "tables de multiplication".
Possibilité de masquer le détail du quadrillage pour passer de la représentation à la modélisation, et d'effectuer des tirages au sort.
*Ce logiciel est à télécharger et à installer (Linux & Windows).*

## Maîtrise du langage

### Syllabux
Syllabux reproduit le fonctionnement du syllabaire papier qu'on trouve sous forme de classeurs, avec les pages consonnes et voyelles qui se tournent indépendamment pour former les syllabes.
- 4 polices de caractères au choix
- choix des phonèmes
- 3 modèles de syllabe : CV, VC, CVC
Composition manuelle avec le syllabaire affiché, semi-automatique (tirage au sort contrôlé par un bouton "suivant") ou automatique (mode diaporama avec choix du tempo.
*Cette application fonctionne en ligne sans installation (tous appareils).*
*Elle existe en version intégrable à Openboard.*

### Flashmots
Flashmots est dédiée à la mémorisation orthographique.
Un mot est affiché (temps de lecture), puis caché (temps de mémorisation), l'élève doit ensuite l'écrire à l'identique.
En fin d'exercice, Flashmots affiche un résumé avec les mots travaillés, le nombre d'essais réalisés pour chacun et le nombre de vues demandées.
Cinq listes de mots personnalisables sont proposées par niveaux du CP au CM2, à titre indicatif.
Il est possible de partager une liste personnalisée par lien, pour créer un lien d'exercice.
*Cette application fonctionne en ligne sans installation (tous appareils).*

### RapidÉtik
RapidÉtik est un outil permettant de transformer une phrase en étiquettes déplaçables, qu'il faut ensuite remettre dans le bon ordre.
On peut aussi créer une phrase à trous.
Les exercices sont partageables par lien, comme pour Flashmots.
*Cette application fonctionne en ligne sans installation (tous appareils).*

### Tuxprompt
Tuxprompt est une application de type prompteur.
Il suffit d'entrer ou de coller un texte (on peut aussi indiquer un lien Framapad) et de choisir la vitesse de défilement.
*Cette application fonctionne en ligne sans installation (tous appareils).*

### Motminot
Motminot est un jeu inspiré de l'ancien jeu télévisé Motus, adapté pour les élèves de niveau élémentaire.
En six essais il faut deviner un mot. On ne peut proposer que des mots du dictionnaire, accords et conjugaisons comprises. Après chaque proposition, Motminot indiquera les lettres correctes et le cas échéant si elles sont bien placées, façon "Mastermind".
Un mot par jour, le même pour tous.
Un thème par semaine.
*Cette application fonctionne en ligne sans installation (tous appareils).*

## Gestion d'école

### Répartition
Répartition est un outil de visualisation et d’organisation pour les répartitions de classes à l’école primaire.
- Renseignez vos effectifs dans les encadrés correspondants.
- L'application propose une première ventilation automatique.
- Ajustez si nécessaire les paramètres.
- Déplacez les effectifs manuellement pour ajuster.
- Téléchargez vos effectifs en CSV (format utilisable avec un tableur).
*Cette application fonctionne en ligne sans installation (tous appareils).*


### Remix
Remix est une application permettant de **former des groupes d'élèves hétérogènes**. Par exemple, à partir d'une classe de CP, de CE1 et de CE2, on peut obtenir 5 groupes contenant des élèves de chaque niveau.
Ce principe peut s'appliquer à d'autres entités que des classes et des élèves, de façon générale Remix permet de ventiler des données.
**Important : les informations sont traitées localement, rien ne remonte sur le serveur, c'est tout comme si cela se passait sur votre tableur.**
*Cette application fonctionne en ligne sans installation (tous appareils).*

## Divers

### Seyes
Seyes affiche une page quadrillée de type cahier, et permet d'y ajouter du texte.
Elle utilise par défaut la police "Belle Allure" mais d'autres sont également disponibles.
On peut changer de type de lignage, et effectuer une mise en forme sommaire (couleurs et soulignement).
Le contenu est sauvegardé localement et peut être partagé par lien.
*Cette application fonctionne en ligne sans installation (tous appareils).*
*Elle existe en version intégrable à Openboard.*

### Minimages
Minimages permet simplement et facilement de réduire un lot d'images.
Idéal pour préparer un projet web : on obtient en deux clics des photos beaucoup plus légères (mais suffisantes pour un usage web) qui occuperont moins de place, se chargeront plus rapidement et limiteront les possibilités de zoom, notamment sur les visages, ce qui peut contribuer à protéger le droit à l'image des élèves. 
*Cette application fonctionne en ligne sans installation (tous appareils).*

### Lis pour moi
Lis pour moi est une application très simple et minimaliste. Elle transforme une phrase en un lien + un code QR, qui aboutit une synthèse vocale.
Cela peut être utilisé comme outil d'adaptation pour accompagner une consigne écrite, par exemple.
*Cette application fonctionne en ligne sans installation (tous appareils).*

### AutoBD
AutoBD permet de réaliser facilement une bande dessinée, et de l’exporter sous forme d’image.
Elle est fournie avec une collection de fonds et de personnages sous licence libre, mais il est possible d'utiliser ses propres contenus depuis le stockage de l'ordinateur.
L'application facilite l'attribution des crédits en apposant automatiquement la mention adéquate à mesure que l'on utilise les dessins fournis. Cela permet de respecter le droit d'auteur plus facilement et y sensibilise les élèves.

*Cette application fonctionne en ligne sans installation (tous appareils).*

### Tux Timer

Tuxtimer est un compte à rebours graphique de 0 à 60 minutes.
Il permet à un élève ou un groupe d'avoir une représentation du temps restant sans savoir lire l'heure sur un cadran.
Lorsque la souris quitte la zone de l'horloge, l'interface est volontairement épurée pour faciliter la concentration.
*Cette application fonctionne en ligne sans installation (tous appareils).*
*Elle existe en version intégrable à Openboard.*